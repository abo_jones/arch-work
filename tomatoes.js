$(document).ready(function() {

	var ctrl = new ScrollMagic.Controller({
		globalSceneOptions: {
			triggerHook: 'onLeave',
			
		}
	});

	$("section").each(function() {
		new ScrollMagic.Scene({
			triggerElement: this
		})
		.setPin(this)
		.loglevel(3)
		.addIndicators({
			colorStart: "rgba(255,255,255,0.5)",
			colorEnd: "rgba(255,255,255,0.5)",
			colorTrigger: "rgba(255,255,255,1)",
			name: name
		})
		.addTo(ctrl);

		var wh = window.innerHeight;

		new ScrollMagic.Scene({
			offset: wh*3
		})
		.setClassToggle("section#four", "is-active")
		.addTo(ctrl);

	});

	$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});


})(jQuery);

















