module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		concat: {
			dist: {
				src:[
					'tomatoes.js'
				],
				dest: "production.js"
			}

		},

		uglify: {
			build: {
				src: 'production.js',
				dest: 'production.min.js'
			}
		},

		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd:'src/',
					src:['**/*.{png,jpg,gif}'],
					dest: 'src/'
				}]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');

	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('default', ['concat', 'uglify', 'imagemin']);
};


























